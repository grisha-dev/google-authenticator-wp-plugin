jQuery(document).ready(function($) {


    // Perform AJAX login on form submit
    $('form#loginform').on('submit', function(e){
        e.preventDefault();

        var redirect_url = $('[name="redirect_to"]').val();
        // $('form#login p.status').show().text(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#loginform #user_login').val(),
                'password': $('form#loginform #user_pass').val(),
                'security': $('form#loginform #security').val()
                },
            success: function(data){
                console.log(data);
                if(!$('#login-status').length) {
                    $('#loginform').prepend('<div id="login-status"></div>');
                }
                $('#login-status').text(data.message);
               if (data.user_check == true) {
                    $('form#token-form, #ga-mask').fadeIn(300);
                }
                else if (data.loggedin == true){
                    document.location.href = redirect_url;
                }
            }
        });

    });
    $('form#token-form').on('submit', function(e){
        e.preventDefault();

        var redirect_url = $('[name="redirect_to"]').val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin1', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#loginform #user_login').val(),
                'password': $('form#loginform #user_pass').val(),
                'googleotp': $('form#token-form #googleotp').val()
            },
            success: function(data){
                console.log(data);
                // $('form#login p.status').text(data.message);

               if (data.loggedin == true){
                    document.location.href = redirect_url;
                }
                else  {
                   $('.info-text').text(data.message)
               }
            }
        });

    })



});
jQuery(document).ready(function($){
    $('form#token-form').center();
    $('body').on('click', '#ga-mask', function () {
        $('#ga-mask , form#token-form').fadeOut(300);
        $('.info-text').text('');
        $('form#token-form #googleotp').val('');
    });
});
jQuery( window ).resize(function() {
    jQuery('form#token-form').center();
});

(function($){
    $.fn.extend({
        center: function (options) {
            var options =  $.extend({ // Default values
                inside:window, // element, center into window
                transition: 0, // millisecond, transition time
                minX:0, // pixel, minimum left element value
                minY:0, // pixel, minimum top element value
                withScrolling:true, // booleen, take care of the scrollbar (scrollTop)
                vertical:true, // booleen, center vertical
                horizontal:true // booleen, center horizontal
            }, options);
            return this.each(function() {
                var props = {position:'absolute'};
                if (options.vertical) {
                    var top = ($(options.inside).height() - $(this).outerHeight()) / 2;
                    if (options.withScrolling) top += $(options.inside).scrollTop() || 0;
                    top = (top > options.minY ? top : options.minY);
                    $.extend(props, {top: top+'px'});
                }
                if (options.horizontal) {
                    var left = ($(options.inside).width() - $(this).outerWidth()) / 2;
                    if (options.withScrolling) left += $(options.inside).scrollLeft() || 0;
                    left = (left > options.minX ? left : options.minX);
                    $.extend(props, {left: left+'px'});
                }
                if (options.transition > 0) $(this).animate(props, options.transition);
                else $(this).css(props);
                return $(this);
            });
        }
    });
})(jQuery)