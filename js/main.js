jQuery(document).ready(function($) {
    $('#GA_newsecret').bind('click', function () {
        // Remove existing QRCode
        $('#GA_QRCODE').html("");
        var data = new Object();
        data['action'] = 'GoogleAuthenticator_action';
        // data['nonce']	= GAnonce;
        $.post(ajax_object.ajaxurl, data, function (response) {
            // console.log(response);
            $('#GA_secret').val(response['new-secret']);
            var qrcode = "otpauth://totp/WordPress:" + escape($('#GA_description').val()) + "?secret=" + $('#GA_secret').val() + "&issuer=WordPress";
            $('#GA_QRCODE').qrcode(qrcode);
            $('#GA_QR_INFO').show('slow');
        });
    });

// If the user starts modifying the description, hide the qrcode
    $('#GA_description').bind('focus blur change keyup', function () {
        $('#GA_QRCODE').html("");
        if ($('#GA_secret').val()) {
            var qrcode = "otpauth://totp/WordPress:" + escape($('#GA_description').val()) + "?secret=" + $('#GA_secret').val() + "&issuer=WordPress";
            $('#GA_QRCODE').qrcode(qrcode);
            $('#GA_QR_INFO').show('slow');
        }

    });

    if ($('#GA_secret').val()) {
        var qrcode = "otpauth://totp/WordPress:" + escape($('#GA_description').val()) + "?secret=" + $('#GA_secret').val() + "&issuer=WordPress";
        $('#GA_QRCODE').qrcode(qrcode);
        $('#GA_QR_INFO').show('slow');
    }



    $('form#ga-settings-form').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_object.ajaxurl,
            data: {
                'action': 'ga_settings_form_handler',
                'username': $(this).find('#username').val(),
                'password': $(this).find('#password').val(),
                'GA_enabled': $(this).find('#GA_enabled').prop('checked'),
                'GA_description': $(this).find('#GA_description').val(),
                'GA_secret': $(this).find('#GA_secret').val()
            },
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            success: function (data) {
                $('#ga-settings-form-status').text(data.message);
                $("#ajax-loader").hide();
                if (data.ga_enabled == 'enabled') {
                    $('.btn-2fa').fadeOut(300);
                }
                else {
                    $('.btn-2fa').fadeIn(300);
                }
                console.log(data);
            }
        });
    });

    $('.btn-2fa').click(function (e) {
        e.preventDefault();

        var btn = $(this);
        $.ajax({
            type: 'POST',
            url: ajax_object.ajaxurl,
            data: {
                'action': 'ga_activate_2fa'
            },
            success: function () {
                btn.fadeOut(300);
            }
        });
    });
});