<?php if(is_user_logged_in()) {

    $user_id = get_current_user_id();

    $GA_secret			= trim( get_user_option( 'googleauthenticator_secret', $user_id ) );
    $GA_enabled			= trim( get_user_option( 'googleauthenticator_enabled', $user_id ) );
    $GA_description		= trim( get_user_option( 'googleauthenticator_description', $user_id ) );


?>

<h3><?php echo __( 'Google Authenticator Settings', 'google-authenticator' ) ?></h3>
<div id="ga-settings-form-status"></div>
<form id="ga-settings-form" name="ga-settings-form" method="post">
    <table class="form-table">
        <tbody>
       <tr>
           <th scope="row"><?php echo __( 'Active', 'google-authenticator' ) ?></th>
            <td>
                <input name="GA_enabled" id="GA_enabled" class="tog" type="checkbox" <?php checked( $GA_enabled, 'enabled');?>>
            </td>
        </tr>
       <tr>
           <th><label for="password"><?php echo __('Password','google-authenticator') ?></label></th>
           <td><input name="password" id="password"  type="password" size="25" required/></td>
       </tr>
       <tr>
          <th><label for="GA_description"><?php echo __('Description','google-authenticator') ?></label></th>
           <td><input name="GA_description" id="GA_description" value="<?php echo $GA_description; ?>" type="text" size="25" /></td>
       </tr>
       <tr>
           <th><label for="GA_secret"><?php echo __('Secret','google-authenticator') ?></label></th>
           <td>
               <input name="GA_secret" id="GA_secret" value="<?php echo $GA_secret;?>" readonly="readonly" type="text"
                      size="25"/>
               <input name="GA_newsecret" id="GA_newsecret" value="<?php echo __("Create new secret",'google-authenticator')?>" type="button"
                      class="button">
<!--               <input name="show_qr" id="show_qr" value="--><?php //echo __("Show/Hide QR code",'google-authenticator') ?><!--" type="button" class="button"-->
<!--                      onclick="ShowOrHideQRCode();" />-->
           </td>
       </tr>
       <tr>
           <th></th>
           <td>
               <div id="GA_QR_INFO" style="display: none">
                   <div id="GA_QRCODE"></div>
                   <span class="description"><br/><?php echo __( 'Scan this with the Google Authenticator app.', 'google-authenticator' ) ?> </span>
                   </div>
           </td>
       </tr>
        <tr>
            <td>
                <input type="submit" value="Save">
                <img id="ajax-loader" src="<?php echo plugins_url('preloader.gif', __FILE__);?>" style="display: none;" />
            </td>
        </tr>
        </tbody>
    </table>
</form>
<?php } else { ?>
 <div class="not-log-message">
     You must log in to use two step authentication <a href="<?php echo wp_login_url(get_permalink( get_page_by_title( 'Two step authentication' ) ));?>">Login</a>
 </div>
<?php } ?>