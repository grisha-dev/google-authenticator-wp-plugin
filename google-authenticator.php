<?php
/*
Plugin Name: Google Authenticator
Description: Two-Factor Authentication for WordPress using the Android/iPhone/Blackberry app as One Time Password generator.
Author: Grisha Yalanskiy
Version: 1
Text Domain: google-authenticator
Domain Path: /lang
*/
register_activation_hook( __FILE__, array( 'GoogleAuthenticator','ga_insert_settings_page') );

class GoogleAuthenticator {

static $instance; // to store a reference to the plugin, allows other plugins to remove actions

/**
 * Constructor, entry point of the plugin
 */
function __construct() {
    self::$instance = $this;
    add_action( 'init', array( $this, 'init' ) );
}

/**
 * Initialization, Hooks, and localization
 */
function init() {

    require_once( 'base32.php' );


    add_shortcode('ga-settings-form', array( $this,'ga_settings_form_callback'));
    add_action( 'login_form', array( $this, 'loginform' ) );
    add_action( 'login_footer', array( $this, 'loginfooter' ) );


    add_action( 'wp_ajax_GoogleAuthenticator_action', array( $this, 'ajax_callback' ) );
    add_action( 'wp_ajax_nopriv_GoogleAuthenticator_action', array( $this, 'ajax_callback' ) );



	add_action('wp_enqueue_scripts', array($this, 'ga_enqueue_scripts'));

    add_action( 'wp_ajax_nopriv_ajaxlogin', array( $this,'ajax_login') );
    add_action( 'wp_ajax_ajaxlogin', array( $this,'ajax_login') );
    add_action( 'wp_ajax_nopriv_ajaxlogin1', array( $this,'ajax_login1') );

    add_action('wp_ajax_ga_settings_form_handler', array( $this,'ga_settings_form_handler'));
	add_action('wp_ajax_ga_autofill_desc_qr', array( $this,'ga_autofill_desc_qr'));
    add_action('wp_ajax_ga_activate_2fa', array( $this,'ga_activate_2fa'));


	add_action('wp_footer', array($this, 'add_2fa_button'), 9);

}

function ga_enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('qrcode_script', plugins_url('jquery.qrcode.min.js', __FILE__),array("jquery"));

    wp_enqueue_script('ga-main-js', plugins_url('js/main.js', __FILE__), array("jquery", "qrcode_script"), null, true);
    wp_localize_script( 'ga-main-js', 'ajax_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),

    ));
}
/**
 * Check the verification code entered by the user.
 */
function verify( $secretkey, $thistry, $relaxedmode, $lasttimeslot ) {

	// Did the user enter 6 digits ?
	if ( strlen( $thistry ) != 6) {
		return false;
	} else {
		$thistry = intval ( $thistry );
	}

	// If user is running in relaxed mode, we allow more time drifting
	// �4 min, as opposed to � 30 seconds in normal mode.
	if ( $relaxedmode == 'enabled' ) {
		$firstcount = -8;
		$lastcount  =  8; 
	} else {
		$firstcount = -1;
		$lastcount  =  1; 	
	}
	
	$tm = floor( time() / 30 );
	
	$secretkey=Base32::decode($secretkey);
	// Keys from 30 seconds before and after are valid aswell.
	for ($i=$firstcount; $i<=$lastcount; $i++) {
		// Pack time into binary string
		$time=chr(0).chr(0).chr(0).chr(0).pack('N*',$tm+$i);
		// Hash it with users secret key
		$hm = hash_hmac( 'SHA1', $time, $secretkey, true );
		// Use last nipple of result as index/offset
		$offset = ord(substr($hm,-1)) & 0x0F;
		// grab 4 bytes of the result
		$hashpart=substr($hm,$offset,4);
		// Unpak binary value
		$value=unpack("N",$hashpart);
		$value=$value[1];
		// Only 32 bits
		$value = $value & 0x7FFFFFFF;
		$value = $value % 1000000;
		if ( $value === $thistry ) {
			// Check for replay (Man-in-the-middle) attack.
			// Since this is not Star Trek, time can only move forward,
			// meaning current login attempt has to be in the future compared to
			// last successful login.
			if ( $lasttimeslot >= ($tm+$i) ) {
				error_log("Google Authenticator plugin: Man-in-the-middle attack detected (Could also be 2 legit login attempts within the same 30 second period)");
				return false;
			}
			// Return timeslot in which login happened.
			return $tm+$i;
		}
	}
	return false;
}

/**
 * Create a new random secret for the Google Authenticator app.
 * 16 characters, randomly chosen from the allowed Base32 characters
 * equals 10 bytes = 80 bits, as 256^10 = 32^16 = 2^80
 */ 
function create_secret() {
    $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'; // allowed characters in Base32
    $secret = '';
    for ( $i = 0; $i < 16; $i++ ) {
        $secret .= substr( $chars, wp_rand( 0, strlen( $chars ) - 1 ), 1 );
    }
    return $secret;
}



/**
 * Add verification code field to login form.
 */
function loginform() {
     wp_nonce_field( 'ajax-login-nonce', 'security' );
}

/**
 * Disable autocomplete on Google Authenticator code input field.
 */
function loginfooter($user) { ?>
		<form name="token-form" id="token-form" action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>"
			  method="post" style="display: none;">
            <div class="info-text"></div>
			<p>
				<label for="token-input"><?php _e('Token 2FA'); ?><br/>
					<input type="text" name="googleotp" id="googleotp" class="input" size="20"/></label>
			</p>
			<input type="submit" class="button button-primary button-large" value="<?php esc_attr_e('Submit'); ?>"/>
		</form>
    <div id="ga-mask"></div>
<?php
    wp_enqueue_style('ga-css', plugins_url('css/ga.css', __FILE__));
	wp_enqueue_script('jquery');
    wp_enqueue_script('ajax-login-script', plugins_url('js/ajax-login-script.js', __FILE__), array('jquery'), null, true);


    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => admin_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));

}

/**
 * Login form handling.
 * Check Google Authenticator verification code, if user has been setup to do so.
 * @param wordpressuser
 * @return user/loginstatus
 */
function check_otp( $user, $username = '', $password = '' ) {
	// Store result of loginprocess, so far.
	$userstate = $user;

	// Get information on user, we need this in case an app password has been enabled,
	// since the $user var only contain an error at this point in the login flow.
	if ( get_user_by( 'email', $username ) === false ) {
		$user = get_user_by( 'login', $username );
	} else {
		$user = get_user_by( 'email', $username );
	}

	// Does the user have the Google Authenticator enabled ?
	if ( isset( $user->ID ) && trim(get_user_option( 'googleauthenticator_enabled', $user->ID ) ) == 'enabled' ) {
		$show_popup = true;
		// Get the users secret
		$GA_secret = trim( get_user_option( 'googleauthenticator_secret', $user->ID ) );
		
		// Figure out if user is using relaxed mode ?
		$GA_relaxedmode = trim( get_user_option( 'googleauthenticator_relaxedmode', $user->ID ) );
		
		// Get the verification code entered by the user trying to login
		if ( !empty( $_POST['googleotp'] )) { // Prevent PHP notices when using app password login
			$otp = trim( $_POST[ 'googleotp' ] );
		} else {
			$otp = '';
		}
		// When was the last successful login performed ?
		$lasttimeslot = trim( get_user_option( 'googleauthenticator_lasttimeslot', $user->ID ) );
		// Valid code ?
		if ( $timeslot = $this->verify( $GA_secret, $otp, $GA_relaxedmode, $lasttimeslot ) ) {
			// Store the timeslot in which login was successful.
			update_user_option( $user->ID, 'googleauthenticator_lasttimeslot', $timeslot, true );
			return $userstate;
		} else {
			// No, lets see if an app password is enabled, and this is an XMLRPC / APP login ?
			if ( trim( get_user_option( 'googleauthenticator_pwdenabled', $user->ID ) ) == 'enabled' && ( defined('XMLRPC_REQUEST') || defined('APP_REQUEST') ) ) {
				$GA_passwords 	= json_decode(  get_user_option( 'googleauthenticator_passwords', $user->ID ) );
				$passwordhash	= trim($GA_passwords->{'password'} );
				$usersha1		= sha1( strtoupper( str_replace( ' ', '', $password ) ) );
				if ( $passwordhash == $usersha1 ) { // ToDo: Remove after some time when users have migrated to new format
					return new WP_User( $user->ID );
				  // Try the new version based on thee wp_hash_password	function
				} elseif (wp_check_password( strtoupper( str_replace( ' ', '', $password ) ), $passwordhash)) {
					return new WP_User( $user->ID );
				} else {
					// Wrong XMLRPC/APP password !
					return new WP_Error( 'invalid_google_authenticator_password', __( '<strong>ERROR</strong>: The Google Authenticator password is incorrect.', 'google-authenticator' ) );
				} 		 
			} else {
				return new WP_Error( 'invalid_google_authenticator_token', __( '<strong>ERROR</strong>: The Google Authenticator code is incorrect or has expired.', 'google-authenticator' ) );
			}	
		}
	}
	// Google Authenticator isn't enabled for this account,
	// just resume normal authentication.
	return $userstate;
}


/**
* AJAX callback function used to generate new secret
*/
function ajax_callback() {
	
	// Create new secret.
	$secret = $this->create_secret();

	$result = array( 'new-secret' => $secret );
	header( 'Content-Type: application/json' );
	echo json_encode( $result );

	die(); 
}
/**
 *  Insert settings page
 */
function ga_insert_settings_page() {

    if (!get_page_by_title( 'Two step authentication' )) {

        $content = '[ga-settings-form]';
        $post = array(
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_author' => get_current_user_id(),
            'post_date' => date('Y-m-d H:i:s'),
            'post_name' => 'two-step-authentication',
            'post_status' => 'publish',
            'post_title' => 'Two step authentication',
            'post_content' => $content,
            'post_type' => 'page',
        );
        $newvalue = wp_insert_post($post, false);
        update_option( 'two-step-authentication', $newvalue );
    }
}

/**
 * AJAX login
 */
function ajax_login(){

    check_ajax_referer( 'ajax-login-nonce', 'security' );

   $user = get_user_by( 'login', $_POST['username'] );

    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    if(trim(get_user_option( 'googleauthenticator_enabled', $user->ID )) == 'enabled') {
        $user_check = wp_authenticate_username_password(null, $info['user_login'], $info['user_password']);
        if (is_wp_error( $user_check )) {
            echo json_encode(array('user_check'=>false, 'message'=>__('Wrong username or password.')));
        }
        else {
            echo json_encode(array('user_check'=>true));
        }
    }
    else {
        $user_signon = wp_signon($info, false);
        if (is_wp_error($user_signon)) {
            echo json_encode(array('loggedin' => false, 'message' => __('Wrong username or password.')));
        } else {
            echo json_encode(array('loggedin' => true));
        }
    }

    exit();
}

function ajax_login1() {
    add_filter('authenticate', array($this, 'check_otp'), 50, 3);
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;
    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => __('Wrong token')));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => __('Login successful, redirecting...')));
    }
    exit();
}

function ga_settings_form_handler() {
    $user_id = get_current_user_id();

    $info = array();
    $info['user_login'] = wp_get_current_user()->user_login;
    $info['user_password'] = $_POST['password'];

    $GA_enabled	=  filter_var($_POST['GA_enabled'],FILTER_VALIDATE_BOOLEAN);
    $GA_description	= trim( sanitize_text_field($_POST['GA_description'] ) );
    $GA_secret	= trim( $_POST['GA_secret'] );

    if ( $GA_enabled ) {
        $GA_enabled = 'enabled';
		$result['message'] = __('2FA Active');
    } else {
        $GA_enabled = 'disabled';
		$result['message'] = __('2FA Inactive');
    }

    $user_check = wp_authenticate_username_password(null, $info['user_login'], $info['user_password']);

    if (!is_wp_error( $user_check )) {
        update_user_option( $user_id, 'googleauthenticator_enabled', $GA_enabled, true );
        update_user_option( $user_id, 'googleauthenticator_description', $GA_description, true );
        update_user_option( $user_id, 'googleauthenticator_secret', $GA_secret, true );
    }
	else {
            $result['message'] = __('Wrong password.');
	}
    $result['ga_enabled'] = $GA_enabled;
	echo json_encode($result);
	exit();
}
	function ga_autofill_desc_qr() {
		$info = array();
		$info['user_login'] = $_POST['username'];
		$info['user_password'] = $_POST['password'];

		$result = array();

		$user_check = wp_authenticate_username_password(null, $info['user_login'], $info['user_password']);
		if (!is_wp_error( $user_check )) {
			$user = get_user_by( 'login', $info['user_login'] );

			if (get_user_option('googleauthenticator_description', $user->ID  )) {
				$result['ga_description'] = get_user_option('googleauthenticator_description', $user->ID  );
			}
			if (get_user_option('googleauthenticator_secret', $user->ID  )) {
				$result['ga_secret'] = get_user_option('googleauthenticator_secret', $user->ID  );
			}
            if(trim(get_user_option( 'googleauthenticator_enabled', $user->ID )) == 'enabled') {
                $result['show_token_input'] = true;
            }


			echo json_encode($result);

		}
		exit();

	}

	function ga_activate_2fa() {
        update_user_option( get_current_user_id(), 'googleauthenticator_enabled', 'enabled', true );
        exit();
    }

	function add_2fa_button(){

        if (is_user_logged_in() && trim(get_user_option( 'googleauthenticator_enabled', get_current_user_id() ) ) == 'disabled' && get_user_option( 'googleauthenticator_lasttimeslot', get_current_user_id() ))  {

            echo '<button class="btn-2fa">Activate<br> 2FA</button>';
            echo '<style>
                    .btn-2fa {
                        position: fixed;
                        right: 0;
                        top: 27%;
                        z-index: 10;
                        font-weight: 600;
                        letter-spacing: 1.3px; 
                        padding: 20px 12px;
                        color: #ffffff;
                        line-height: 1.2;           
                        display: inline-block;
                        border-radius: 5px 0 0 5px;
                        cursor: pointer;
                    }
                </style>';
        }
    }

    function ga_settings_form_callback() {

        $output = '';
        ob_start();
        include('ga-form.php');
        $output .= ob_get_clean();

        return $output;
    }



} // end class

$google_authenticator = new GoogleAuthenticator;
?>
